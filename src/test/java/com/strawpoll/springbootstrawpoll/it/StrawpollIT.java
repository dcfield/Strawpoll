package com.strawpoll.springbootstrawpoll.it;

import com.strawpoll.springbootstrawpoll.model.Strawpoll;
import com.strawpoll.springbootstrawpoll.restfulApi.StrawpollRestfulApi;
import org.glassfish.jersey.internal.Errors;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import javax.ws.rs.core.Response;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Java6Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class StrawpollIT {

	@Autowired
	private StrawpollRestfulApi strawpollRestfulApi;

	@Test
	public void shouldFetchAllUsers() throws Exception{
		List<Strawpoll> strawpolls = strawpollRestfulApi.fetchStrawpolls(null);
		assertThat(strawpolls).hasSize(1);

		Strawpoll testStrawpoll = new Strawpoll(
				null, "My strawpoll", new String[]{"Question 1", "Question 2"}
		);

		assertThat(strawpolls.get(0)).isEqualToIgnoringGivenFields(testStrawpoll, "strawpollUid");
		assertThat(strawpolls.get(0).getStrawpollUid()).isInstanceOf(UUID.class);
		assertThat(strawpolls.get(0).getStrawpollUid()).isNotNull();
	}

	@Test
	public void shouldInsertStrawpoll() throws Exception{
		// Given
		UUID strawpollUid = UUID.randomUUID();
		Strawpoll testStrawpoll = new Strawpoll(
				strawpollUid, "My strawpoll", new String[]{"Question 1", "Question 2"}
		);

		// When
		strawpollRestfulApi.insertNewStrawpoll(testStrawpoll);

		// Then
		ResponseEntity response = strawpollRestfulApi.fetchStrawpoll(strawpollUid);
		assertThat(response.getBody()).isEqualToComparingFieldByField(testStrawpoll);
	}

	@Test
	public void shouldDeleteStrawpoll() throws Exception{
		// Given
		UUID strawpollUid = UUID.randomUUID();
		Strawpoll testStrawpoll = new Strawpoll(
				strawpollUid, "My strawpoll", new String[]{"Question 1", "Question 2"}
		);

		// When
		strawpollRestfulApi.insertNewStrawpoll(testStrawpoll);

		// Then
		ResponseEntity response = strawpollRestfulApi.fetchStrawpoll(strawpollUid);
		assertThat(response.getBody()).isEqualToComparingFieldByField(testStrawpoll);

		// When
		strawpollRestfulApi.deleteStrawpoll(strawpollUid);

		// Then
		response = strawpollRestfulApi.fetchStrawpoll(strawpollUid);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
	}

	@Test
	public void shouldUpdateStrawpoll() throws Exception{
		// Given
		UUID strawpollUid = UUID.randomUUID();
		Strawpoll testStrawpoll = new Strawpoll(
				strawpollUid, "My strawpoll", new String[]{"Question 1", "Question 2"}
		);

		// When
		strawpollRestfulApi.insertNewStrawpoll(testStrawpoll);

		Strawpoll updatedStrawpoll = new Strawpoll(
				strawpollUid, "My updated strawpoll", new String[]{"Question 1 updated", "Question 2 updated"}
		);

		strawpollRestfulApi.updateStrawpoll(updatedStrawpoll);

		// Then
		ResponseEntity response = strawpollRestfulApi.fetchStrawpoll(strawpollUid);
		assertThat(response.getBody()).isEqualToComparingFieldByField(updatedStrawpoll);
	}

	@Test
	public void shouldFetchStrawpollsByTitle() throws Exception {
		// Given
		UUID strawpollUid = UUID.randomUUID();
		Strawpoll testStrawpoll = new Strawpoll(
				strawpollUid, "My custom strawpoll", new String[]{"Cusstom question 1", "Custom question 2"}
		);

		// When
		strawpollRestfulApi.insertNewStrawpoll(testStrawpoll);

		List<Strawpoll> strawpollsByTitle = strawpollRestfulApi.fetchStrawpolls("My strawpoll");

		// Then
		assertThat(strawpollsByTitle).extracting("strawpollUid").doesNotContain(strawpollUid);
		assertThat(strawpollsByTitle).extracting("title").doesNotContain(testStrawpoll.getTitle());
		assertThat(strawpollsByTitle).extracting("questions").doesNotContain(testStrawpoll.getQuestions());

	}

}
