package com.strawpoll.springbootstrawpoll.dao;

import com.strawpoll.springbootstrawpoll.model.Strawpoll;
import org.junit.Before;
import org.junit.Test;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Java6Assertions.assertThat;


public class FakeDataDaoTest {

    private FakeDataDao fakeDataDao;

    @Before
    public void setUp() throws Exception {
        fakeDataDao = new FakeDataDao();
    }

    @Test
    public void shouldSelectAllStrawpolls() throws Exception{
        List<Strawpoll> strawpolls = fakeDataDao.selectAllStrawpolls();
        assertThat(strawpolls).hasSize(1);

        Strawpoll strawpoll = strawpolls.get(0);

        assertThat(strawpoll.getTitle()).isEqualTo("My strawpoll");
        assertThat(strawpoll.getQuestions()).isEqualTo(new String[]{"Question 1", "Question 2"});
        assertThat(strawpoll.getStrawpollUid()).isNotNull();
    }

    @Test
    public void shouldSelectStrawpollByStrawpollUid() {
        UUID happinessStrawpollUuid = UUID.randomUUID();
        Strawpoll happiness = new Strawpoll(happinessStrawpollUuid, "My poll 2", new String[]{"How are you", "Good thanks"});
        fakeDataDao.insertStrawpoll(happinessStrawpollUuid, happiness);

        assertThat(fakeDataDao.selectAllStrawpolls()).hasSize(2);

        Optional<Strawpoll> optionalStrawpoll = fakeDataDao.selectStrawpollByStrawpollUid(happinessStrawpollUuid);
        assertThat(optionalStrawpoll.isPresent()).isTrue();
        assertThat(optionalStrawpoll.get()).isEqualToComparingFieldByField(happiness);
    }

    @Test
    public void shouldNotSelectStrawpollByRandomStrawpollUid() {
        Optional<Strawpoll> optionalStrawpoll = fakeDataDao.selectStrawpollByStrawpollUid(UUID.randomUUID());
        assertThat(optionalStrawpoll.isPresent()).isFalse();
    }

    @Test
    public void shouldUpdateStrawpoll() {
        UUID myStrawpollUid = fakeDataDao.selectAllStrawpolls().get(0).getStrawpollUid();
        Strawpoll newStrawpoll = new Strawpoll(
                myStrawpollUid,
                "My poll 3",
                new String[]{"q1", "q2"}
                );
        fakeDataDao.updateStrawpoll(newStrawpoll);
        Optional<Strawpoll> optionalStrawpoll = fakeDataDao.selectStrawpollByStrawpollUid(myStrawpollUid);
        assertThat(optionalStrawpoll.isPresent()).isTrue();

        assertThat(fakeDataDao.selectAllStrawpolls()).hasSize(1);
        assertThat(optionalStrawpoll.get()).isEqualToComparingFieldByField(newStrawpoll);
    }

    @Test
    public void shouldDeleteStrawpollByStrawpollUid() {
        UUID myStrawpollUid = fakeDataDao.selectAllStrawpolls().get(0).getStrawpollUid();
        fakeDataDao.deleteStrawpollByStrawpollUid(myStrawpollUid);
        assertThat(fakeDataDao.selectStrawpollByStrawpollUid(myStrawpollUid).isPresent()).isFalse();
        assertThat(fakeDataDao.selectAllStrawpolls().isEmpty());
    }

    @Test
    public void shouldInsertStrawpoll() {
        UUID strawpollUid = UUID.randomUUID();
        Strawpoll newStrawpoll = new Strawpoll(
                strawpollUid,
                "My poll 4",
                new String[]{"q33", "q44"}
        );

        fakeDataDao.insertStrawpoll(strawpollUid, newStrawpoll);

        assertThat(fakeDataDao.selectStrawpollByStrawpollUid(strawpollUid).isPresent()).isTrue();
        assertThat(fakeDataDao.selectStrawpollByStrawpollUid(strawpollUid).get()).isEqualToComparingFieldByField(newStrawpoll);
        assertThat(fakeDataDao.selectAllStrawpolls()).hasSize(2);
    }
}