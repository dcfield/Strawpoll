package com.strawpoll.springbootstrawpoll.service;

import com.strawpoll.springbootstrawpoll.dao.FakeDataDao;
import com.strawpoll.springbootstrawpoll.model.Strawpoll;
import jersey.repackaged.com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.InvalidClassException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.assertj.core.api.Java6Assertions.assertThatThrownBy;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class StrawpollServiceTest {

    @Mock
    private FakeDataDao fakeDataDao;

    private StrawpollService strawpollService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        strawpollService = new StrawpollService(fakeDataDao);
    }

    @Test
    public void shouldGetAllStrawpolls() {
        UUID strawpollUid = UUID.randomUUID();

        Strawpoll testStrawpoll = new Strawpoll(
                strawpollUid,
                "My poll 4",
                new String[]{"q33", "q44"}
        );
        ImmutableList<Strawpoll> strawpolls = new ImmutableList.Builder<Strawpoll>()
                .add(testStrawpoll)
                .build();

        given(fakeDataDao.selectAllStrawpolls()).willReturn(strawpolls);


        List<Strawpoll> allStrawpolls = strawpollService.getAllStrawpolls(Optional.empty());

        assertThat(allStrawpolls).hasSize(1);

        Strawpoll strawpoll = allStrawpolls.get(0);
        assertStrawpollFields(strawpoll);
    }

    @Test
    public void shouldGetAllStrawpollsByTitle() throws Exception{

        UUID strawpollUid_1 = UUID.randomUUID();
        Strawpoll testStrawpoll_1 = new Strawpoll(
                strawpollUid_1,
                "My poll 4",
                new String[]{"q33", "q44"}
        );

        UUID strawpollUid_2 = UUID.randomUUID();
        Strawpoll testStrawpoll_2 = new Strawpoll(
                strawpollUid_2,
                "TestStrawpoll2",
                new String[]{"test2a", "test2b"}
        );

        ImmutableList<Strawpoll> strawpolls = new ImmutableList.Builder<Strawpoll>()
                .add(testStrawpoll_1)
                .add(testStrawpoll_2)
                .build();

        given(fakeDataDao.selectAllStrawpolls()).willReturn(strawpolls);

        List<Strawpoll> filteredStrawpolls = strawpollService.getAllStrawpolls(Optional.of("My poll 4"));
        assertThat(filteredStrawpolls).hasSize(1);
        assertStrawpollFields(filteredStrawpolls.get(0));
    }

    @Test
    public void shouldGetStrawpoll() throws Exception{
        UUID strawpollUid = UUID.randomUUID();

        Strawpoll testStrawpoll = new Strawpoll(
                strawpollUid,
                "My poll 4",
                new String[]{"q33", "q44"}
        );

        given(fakeDataDao.selectStrawpollByStrawpollUid(strawpollUid)).willReturn(Optional.of(testStrawpoll));

        Optional<Strawpoll> strawpollOptional = strawpollService.getStrawpoll(strawpollUid);

        assertThat(strawpollOptional.isPresent()).isTrue();
        Strawpoll strawpoll = strawpollOptional.get();

        assertStrawpollFields(strawpoll);
    }

    @Test
    public void shouldUpdateStrawpoll() {
        UUID strawpollUid = UUID.randomUUID();

        Strawpoll testStrawpoll = new Strawpoll(
                strawpollUid,
                "My poll 4",
                new String[]{"q33", "q44"}
        );

        given(fakeDataDao.selectStrawpollByStrawpollUid(strawpollUid)).willReturn(Optional.of(testStrawpoll));
        given(fakeDataDao.updateStrawpoll(testStrawpoll)).willReturn(1);

        ArgumentCaptor<Strawpoll> captor = ArgumentCaptor.forClass(Strawpoll.class);

        int updateResult = strawpollService.updateStrawpoll(testStrawpoll);

        verify(fakeDataDao).selectStrawpollByStrawpollUid(strawpollUid);
        verify(fakeDataDao).updateStrawpoll(captor.capture());

        Strawpoll strawpoll = captor.getValue();
        assertStrawpollFields(strawpoll);


        assertThat(updateResult).isEqualTo(1);

    }

    @Test
    public void shouldRemoveStrawpoll() {
        UUID strawpollUid = UUID.randomUUID();

        Strawpoll testStrawpoll = new Strawpoll(
                strawpollUid,
                "My poll 4",
                new String[]{"q33", "q44"}
        );

        given(fakeDataDao.selectStrawpollByStrawpollUid(strawpollUid)).willReturn(Optional.of(testStrawpoll));
        given(fakeDataDao.deleteStrawpollByStrawpollUid(strawpollUid)).willReturn(1);

        int deleteResult = strawpollService.removeStrawpoll(strawpollUid);

        verify(fakeDataDao).selectStrawpollByStrawpollUid(strawpollUid);
        verify(fakeDataDao).deleteStrawpollByStrawpollUid(strawpollUid);

        assertThat(deleteResult).isEqualTo(1);
    }

    @Test
    public void shouldInsertStrawpoll() {

        Strawpoll testStrawpoll = new Strawpoll(
                null,
                "My poll 4",
                new String[]{"q33", "q44"}
        );

        when(fakeDataDao.insertStrawpoll(isA(UUID.class), isA(Strawpoll.class))).thenReturn(1);

        int insertResult = strawpollService.insertStrawpoll(testStrawpoll);

        ArgumentCaptor<Strawpoll> captor = ArgumentCaptor.forClass(Strawpoll.class);


        verify(fakeDataDao).insertStrawpoll(any(UUID.class), captor.capture());

        Strawpoll strawpoll = captor.getValue();

        assertStrawpollFields(strawpoll);

        assertThat(insertResult).isEqualTo(1);
    }

    private void assertStrawpollFields(Strawpoll strawpoll) {
        assertThat(strawpoll.getTitle()).isEqualTo("My poll 4");
        assertThat(strawpoll.getQuestions()).isEqualTo(new String[]{"q33", "q44"});
        assertThat(strawpoll.getStrawpollUid()).isNotNull();
        assertThat(strawpoll.getStrawpollUid()).isInstanceOf(UUID.class);
    }
}