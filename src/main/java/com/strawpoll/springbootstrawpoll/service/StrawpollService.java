package com.strawpoll.springbootstrawpoll.service;

import com.strawpoll.springbootstrawpoll.dao.FakeDataDao;
import com.strawpoll.springbootstrawpoll.dao.StrawpollDao;
import com.strawpoll.springbootstrawpoll.model.Strawpoll;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class StrawpollService {

    private FakeDataDao strawpollDao;

    @Autowired
    public StrawpollService(FakeDataDao strawpollDao){
        this.strawpollDao = strawpollDao;
    }

    public List<Strawpoll> getAllStrawpolls(Optional<String> title){
        List<Strawpoll> strawpolls = strawpollDao.selectAllStrawpolls();
        if(!title.isPresent()){
            return strawpolls;
        }

        String myTitle = title.get();
        return strawpolls.stream()
                .filter(strawpoll -> strawpoll.getTitle().equals(myTitle))
                .collect(Collectors.toList());

    }

    public Optional<Strawpoll> getStrawpoll(UUID strawpollUid) {
        return strawpollDao.selectStrawpollByStrawpollUid(strawpollUid);
    }

    public int updateStrawpoll(Strawpoll strawpoll) {
        Optional<Strawpoll> optionalStrawpoll = getStrawpoll(strawpoll.getStrawpollUid());

        if(optionalStrawpoll.isPresent()){
            return strawpollDao.updateStrawpoll(strawpoll);
        }

        return -1;
    }


    public int removeStrawpoll(UUID strawpollUid) {
        Optional<Strawpoll> optionalStrawpoll = getStrawpoll(strawpollUid);

        if(optionalStrawpoll.isPresent()){
            return strawpollDao.deleteStrawpollByStrawpollUid(strawpollUid);

        }
        return -1;
    }


    public int insertStrawpoll(Strawpoll strawpoll) {
        UUID strawpollUid = strawpoll.getStrawpollUid() == null ? UUID.randomUUID() : strawpoll.getStrawpollUid();
        return strawpollDao.insertStrawpoll(strawpollUid, Strawpoll.newStrawpoll(strawpollUid, strawpoll));
    }
}
