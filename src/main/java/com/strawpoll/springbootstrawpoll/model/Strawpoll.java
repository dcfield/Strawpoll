package com.strawpoll.springbootstrawpoll.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Arrays;
import java.util.UUID;

public class Strawpoll {

    private final UUID strawpollUid;
    private final String title;
    private final String[] questions;

    public static Strawpoll newStrawpoll(UUID strawpollUid,Strawpoll strawpoll){
        return new Strawpoll(strawpollUid, strawpoll.getTitle(), strawpoll.getQuestions());
    }

    public Strawpoll(
            @JsonProperty("strawpollUid") UUID strawpollUid,
            @JsonProperty("title") String title,
            @JsonProperty("questions") String[] questions
    ) {
        this.strawpollUid = strawpollUid;
        this.title = title;
        this.questions = questions;
    }

    @JsonProperty("id")
    public UUID getStrawpollUid() {
        return strawpollUid;
    }


    public String getTitle() {
        return title;
    }

    public String[] getQuestions() {
        return questions;
    }

    @JsonIgnore
    public String getFullTitle(){
        return "test_" + title;
    }

    @JsonIgnore
    public int getFullTitleLength(){
        return getFullTitle().length();
    }

    @Override
    public String toString() {
        return "Strawpoll{" +
                "strawpollUid=" + strawpollUid +
                ", title='" + title + '\'' +
                ", questions=" + Arrays.toString(questions) +
                '}';
    }


}
