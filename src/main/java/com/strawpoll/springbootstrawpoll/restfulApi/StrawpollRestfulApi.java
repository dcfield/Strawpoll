/**
 * Implements basic services for RestfulAPI
 *
 * @version 1.0
 * @author dcaulfield
 */
package com.strawpoll.springbootstrawpoll.restfulApi;

import com.strawpoll.springbootstrawpoll.model.Strawpoll;
import com.strawpoll.springbootstrawpoll.service.StrawpollService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.QueryParam;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 *
 */
@RestController
@RequestMapping(
        path = "/api/v1/strawpolls"
)
public class StrawpollRestfulApi {

    private StrawpollService strawpollService;

    @Autowired
    public StrawpollRestfulApi(StrawpollService strawpollService){
        this.strawpollService = strawpollService;
    }

    @RequestMapping(
            method= RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @CrossOrigin(origins = "http://localhost:3000")
    public List<Strawpoll> fetchStrawpolls(
            @QueryParam("title") String title
    ){
        return strawpollService.getAllStrawpolls(Optional.ofNullable(title));
    }

    @RequestMapping(
            method = RequestMethod.GET,
            path = "{strawpollUid}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @CrossOrigin(origins = "http://localhost:3000")
    public ResponseEntity<?> fetchStrawpoll(@PathVariable("strawpollUid") UUID strawpollUid) throws Exception{
        return strawpollService.getStrawpoll(strawpollUid).<ResponseEntity<?>>map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(new ErrorMessage("Strawpoll " + strawpollUid + " not found")));
    }

    @RequestMapping(
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @CrossOrigin(origins = "http://localhost:3000")
    public ResponseEntity<Integer> insertNewStrawpoll(@RequestBody Strawpoll strawpoll){
        int result = strawpollService.insertStrawpoll(strawpoll);
        return getIntegerResponseEntity(result);
    }

    private ResponseEntity<Integer> getIntegerResponseEntity(int result) {
        if(result == 1){
            return ResponseEntity.ok().build();
        }

        return ResponseEntity.badRequest().build();
    }

    @RequestMapping(
            method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @CrossOrigin(origins = "http://localhost:3000")
    public ResponseEntity<Integer> updateStrawpoll(@RequestBody Strawpoll strawpoll){
        int result = strawpollService.updateStrawpoll(strawpoll);

        return getIntegerResponseEntity(result);
    }

    @RequestMapping(
            method = RequestMethod.DELETE,
            path = "{strawpollUid}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @CrossOrigin(origins = "http://localhost:3000")
    public ResponseEntity<Integer> deleteStrawpoll(@PathVariable("strawpollUid") UUID strawpollUid){
        int result = strawpollService.removeStrawpoll(strawpollUid);

        return getIntegerResponseEntity(result);
    }

    class ErrorMessage{
        String errorMessage;

        public ErrorMessage(String errorMessage) {
            this.errorMessage = errorMessage;
        }

        public String getErrorMessage() {
            return errorMessage;
        }

        public void setErrorMessage(String errorMessage) {
            this.errorMessage = errorMessage;
        }
    }
}
