package com.strawpoll.springbootstrawpoll.dao;

import com.strawpoll.springbootstrawpoll.model.Strawpoll;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class FakeDataDao implements StrawpollDao {

    private Map<UUID, Strawpoll> database;

    public FakeDataDao(){
        database = new HashMap<>();
        UUID strawpollUid = UUID.randomUUID();
        database.put(strawpollUid, new Strawpoll(strawpollUid, "My strawpoll", new String[]{"Question 1", "Question 2"}));
    }

    @Override
    public List<Strawpoll> selectAllStrawpolls() {
        return new ArrayList<>(database.values());
    }

    @Override
    public Optional<Strawpoll> selectStrawpollByStrawpollUid(UUID strawpollUid) {
        return Optional.ofNullable(database.get(strawpollUid));
    }

    @Override
    public int updateStrawpoll(Strawpoll strawpoll) {
        database.put(strawpoll.getStrawpollUid(), strawpoll);
        return 1;
    }

    @Override
    public int deleteStrawpollByStrawpollUid(UUID strawpollUid) {
        database.remove(strawpollUid);
        return 1;
    }

    @Override
    public int insertStrawpoll(UUID strawpollUid, Strawpoll strawpoll) {
        database.put(strawpollUid, strawpoll);
        return 1;
    }
}
