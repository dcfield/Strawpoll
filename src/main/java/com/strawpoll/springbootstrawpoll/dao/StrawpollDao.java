package com.strawpoll.springbootstrawpoll.dao;

import com.strawpoll.springbootstrawpoll.model.Strawpoll;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface StrawpollDao {

    List<Strawpoll> selectAllStrawpolls();

    Optional<Strawpoll> selectStrawpollByStrawpollUid(UUID strawpollUid);

    int updateStrawpoll(Strawpoll strawpoll);

    int deleteStrawpollByStrawpollUid(UUID strawpollUid);

    int insertStrawpoll(UUID stawpollUid, Strawpoll strawpoll);
}
